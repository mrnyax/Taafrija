package taafrija.mob.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by mrnyax on 3/25/17.
 */

public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "taafrija.db";
    private static final int DATABASE_VERSION = 1;

    /**
     * Tables Names
     */
    private static final String TABLE_USER_PROFILE = "userProfile";

    /**
     * Table Columns
     */
    private static final String COLUMN_USER_ID = "id";
    private static final String COLUMN_USER_NAME = "userName";
    private static final String COLUMN_PROFILE_PIC_URI = "picUri";

    private static final String TAG = DBHelper.class.getSimpleName();

    /**
     * Default Constructor
     *
     * @param context Application Context
     */

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_USER_PROFILE + "("
                + COLUMN_USER_ID + " INTEGER PRIMARY KEY,"
                + COLUMN_USER_NAME + " TEXT,"
                + COLUMN_PROFILE_PIC_URI + " TEXT" + ")";


        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_PROFILE);

        // Create tables again
        onCreate(db);
    }
}
