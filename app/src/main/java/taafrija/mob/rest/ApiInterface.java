package taafrija.mob.rest;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import taafrija.mob.models.Artist;
import taafrija.mob.models.Event;

/**
 * Created by Patrick on 6/12/2017.
 */

public interface ApiInterface {

    @POST("users")
    Call<Object> saveUserAndCategories(@Body Map<String, Object> registration);

    @GET("events")
    Call<List<Event>> getUserEvents(@Query("user_id") String user_id);

    @GET("nearby")
    Call<List<Event>> getNearbyEvents(@Query("lat") double lat, @Query("lon") double lon);

    @GET("artists")
    Call<List<Artist>> getArtists();
}
