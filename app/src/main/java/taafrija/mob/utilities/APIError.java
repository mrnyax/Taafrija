package taafrija.mob.utilities;

/**
 * Created by Patrick on 6/12/2017.
 */

public class APIError {

    private boolean success;
    private String message;

    public APIError() {
    }

    public APIError(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
