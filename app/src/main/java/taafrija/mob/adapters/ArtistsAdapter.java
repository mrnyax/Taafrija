package taafrija.mob.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import taafrija.mob.R;
import taafrija.mob.activities.ArtistDetailsActivity;
import taafrija.mob.models.Artist;

/**
 * Created by mrnyax on 3/25/17.
 */

public class ArtistsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected Context context;
    protected int lastPosition = -1;
    private List<Artist> artistList;

    public ArtistsAdapter(Context context, List<Artist> list) {
        this.context = context;
        this.artistList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v = inflater.inflate(R.layout.artist_item, viewGroup, false);
        viewHolder = new ArtistsAdapter.ViewHolder(v);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

//        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
//        holder.itemView.startAnimation(animation);
        final Artist artist = artistList.get(position);
        ViewHolder v = (ViewHolder) holder;
        v.name.setText(artist.getArtist_Name());
        Glide.with(context).load("http://taafrija.com/uploads/" + artist.getImg()).centerCrop().into(v.imageView);
        Log.e(" >> ", "http://taafrija.com/uploads/" + artist.getImg());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(context.getApplicationContext(), ArtistDetailsActivity.class);
                a.putExtra("artist", artist.getArtist_Name());
                a.putExtra("desc", artist.getDescription());
                a.putExtra("image", "http://taafrija.com/uploads/" + artist.getImg());
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(a);
            }
        });
    }

    @Override
    public int getItemCount() {
        return artistList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.image);
            name = (TextView) v.findViewById(R.id.artist_name);
        }
    }
}
