package taafrija.mob.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import taafrija.mob.R;
import taafrija.mob.activities.EventDetailsActivity;
import taafrija.mob.models.Event;

/**
 * Created by mrnyax on 3/25/17.
 */

public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    protected Context context;
    protected int lastPosition = -1;
    private List<Event> eventList;

    public MainAdapter(Context context, List<Event> list) {
        this.context = context;
        this.eventList = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        View v = inflater.inflate(R.layout.content_item, viewGroup, false);
        viewHolder = new MainAdapter.ViewHolder(v);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

//        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
//        holder.itemView.startAnimation(animation);
        final Event event = eventList.get(position);
        ViewHolder v = (ViewHolder) holder;
        v.title.setText(event.getTitle());
        Glide.with(context).load("http://taafrija.com/uploads/" + event.getAvatar()).centerCrop().into(v.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(context.getApplicationContext(), EventDetailsActivity.class);
                a.putExtra("title", event.getTitle());
                a.putExtra("desc", event.getDescription());
                a.putExtra("image", "http://taafrija.com/uploads/" + event.getAvatar());
                a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(a);
            }
        });

    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView imageView;

        public ViewHolder(View v) {
            super(v);
            imageView = (ImageView) v.findViewById(R.id.imgView);
            title = (TextView) v.findViewById(R.id.tvTitle);
        }

//        public void bind(final PictureItem item, final OnItemClickListener listener){
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    listener.onItemClick(item);
//                }
//            });
//        }
    }

}

