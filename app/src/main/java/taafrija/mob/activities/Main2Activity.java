package taafrija.mob.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import taafrija.mob.R;

public class Main2Activity extends AppCompatActivity implements LocationListener {

    // Declaring a Location Manager
    protected LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

//        Criteria criteria = new Criteria();
//        criteria.setPowerRequirement(Criteria.POWER_HIGH); // Chose your desired power consumption level.
//        criteria.setAccuracy(Criteria.ACCURACY_FINE); // Choose your accuracy requirement.
//        criteria.setSpeedRequired(true); // Chose if speed for first location fix is required.
//        criteria.setAltitudeRequired(false); // Choose if you use altitude.
//        criteria.setBearingRequired(false); // Choose if you use bearing.
//        criteria.setCostAllowed(false); // Choose if this provider can waste money :-)
//
//        locationManager.getBestProvider(criteria, true);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.e(" >> ", "denied....");
            return;
        }

        try {
            locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, this, Looper.getMainLooper());
            Log.e(" >> ", "accepted....");
        }catch (Exception ex){
            Log.e(" >> ", "error....");
            ex.printStackTrace();
        }

    }


    @Override
    public void onLocationChanged(Location location) {
        Log.e(" >> ", location.getLatitude() + ", " + location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
