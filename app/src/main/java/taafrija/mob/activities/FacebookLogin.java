package taafrija.mob.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import taafrija.mob.R;
import taafrija.mob.constants.Constants;

public class FacebookLogin extends AppCompatActivity {

    public static FacebookLogin instance = null;

    private CallbackManager callbackManager;
    private SharedPreferences sharedPreferences;

    @Override
    public void finish() {
        super.finish();
        instance = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;

        callbackManager = CallbackManager.Factory.create();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_facebook_login);

        sharedPreferences = getSharedPreferences(Constants.PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);

        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("public_profile");
        loginButton.setReadPermissions("email");
        loginButton.setReadPermissions("user_friends");

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("Success", "Success");

                SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                final SharedPreferences.Editor editor = sharedPref.edit();

                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        final Profile profile = Profile.getCurrentProfile();
                        if (profile != null) {

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString(Constants.FACEBOOK_ID, profile.getId());
                            editor.putString(Constants.FACEBOOK_USERNAME, profile.getFirstName() + " " + profile.getLastName());
                            editor.putString(Constants.FACEBOOK_PIC_URL, String.valueOf(profile.getProfilePictureUri(1400, 1400)));
                            editor.commit();
                        }
                        startActivity(new Intent(FacebookLogin.this, SelectCategories.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.e("onCancel", "onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("error", "error: " + error.getMessage());
            }
        });

//        try {
//            PackageInfo info = getApplicationContext().getPackageManager().getPackageInfo(
//                    "taafrija.mob",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.e("KeyHash", "KeyHash:" + Base64.encodeToString(md.digest(),
//                        Base64.DEFAULT));
//                Toast.makeText(this, Base64.encodeToString(md.digest(), Base64.DEFAULT), Toast.LENGTH_LONG).show();
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
