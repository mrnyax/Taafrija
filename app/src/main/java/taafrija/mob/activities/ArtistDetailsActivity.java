package taafrija.mob.activities;

import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import taafrija.mob.R;

public class ArtistDetailsActivity extends AppCompatActivity {

    private String title, body, url;
    private TextView bodyEt, titleEt;
    private ImageView image;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_details);

        toolbar = (Toolbar) findViewById(R.id.anim_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_trans80));
        }

        bodyEt = (TextView) findViewById(R.id.newsText);
        titleEt = (TextView) findViewById(R.id.title);
        image = (ImageView) findViewById(R.id.article_det);

        Intent x = getIntent();
        title = x.getStringExtra("artist");
        body = x.getStringExtra("desc");
        url = x.getStringExtra("image");

        titleEt.setText(title);

        Glide.with(getApplicationContext()).load(url).centerCrop().into(image);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            bodyEt.setText(Html.fromHtml(body, Html.FROM_HTML_MODE_COMPACT));
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            bodyEt.setText(Html.fromHtml(body));
        }
    }
}
