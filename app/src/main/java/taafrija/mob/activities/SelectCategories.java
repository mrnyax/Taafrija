package taafrija.mob.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taafrija.mob.R;
import taafrija.mob.constants.Constants;
import taafrija.mob.rest.ApiClient;
import taafrija.mob.rest.ApiInterface;
import taafrija.mob.utilities.APIError;
import taafrija.mob.utilities.Alerts;
import taafrija.mob.utilities.ErrorUtils;

public class SelectCategories extends AppCompatActivity {

    private LinearLayout linearLayout_done;
    private SharedPreferences sharedPreferences;
    private ToggleButton musicTB, artsTB, theaterTB, festivalTB, partyTB, comedyTB;
    private ArrayList<ToggleButton> toggleButtons;
    private ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    private ProgressDialog progressDialog;
    private Alerts alerts;
    private String facebookID;

    private List<Map<String, Object>> categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_select_categories);

        alerts = new Alerts();
        sharedPreferences = getSharedPreferences(Constants.PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        facebookID = sharedPreferences.getString(Constants.FACEBOOK_ID, "");
        categories = new ArrayList<>();

        musicTB = (ToggleButton) findViewById(R.id.music);
        artsTB = (ToggleButton) findViewById(R.id.arts);
        theaterTB = (ToggleButton) findViewById(R.id.theatre);
        festivalTB = (ToggleButton) findViewById(R.id.festival);
        partyTB = (ToggleButton) findViewById(R.id.party);
        comedyTB = (ToggleButton) findViewById(R.id.comedy);

        toggleButtons = new ArrayList<>();
        toggleButtons.add(musicTB);
        toggleButtons.add(artsTB);
        toggleButtons.add(theaterTB);
        toggleButtons.add(festivalTB);
        toggleButtons.add(partyTB);
        toggleButtons.add(comedyTB);

        linearLayout_done = (LinearLayout) findViewById(R.id.done);
        linearLayout_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                categories.clear();

                for (ToggleButton tb : toggleButtons) {
                    if (tb.isChecked()) {
                        Map<String, Object> map = new HashMap<>();
                        String name = getResources().getResourceEntryName(tb.getId());
                        map.put("cat_id", getCategoryId(name));
                        categories.add(map);
                    }
                }

                Map<String, Object> object = new HashMap<>();
                object.put("id", facebookID);
                object.put("categories", categories);

                if(categories.size() > 0){

                    progressDialog = new ProgressDialog(SelectCategories.this);
                    progressDialog.setMessage("Processing....");
                    progressDialog.setCancelable(false);
                    progressDialog.setIndeterminate(true);
                    progressDialog.show();

                    Call<Object> call = apiService.saveUserAndCategories(object);
                    call.enqueue(new Callback<Object>() {
                        @Override
                        public void onResponse(Call<Object> call, Response<Object> response) {
                            progressDialog.dismiss();
                            if (response.isSuccessful()) {
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString(Constants.CATS_SELECTED, "00");
                                editor.commit();
                                startActivity(new Intent(SelectCategories.this, MainActivity.class));
                                finish();
                            } else {
                                APIError error = ErrorUtils.parseError(response);
                                alerts.showDialog(SelectCategories.this, "Message", error.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(Call<Object> call, Throwable t) {
                            progressDialog.dismiss();
                            alerts.showDialog(SelectCategories.this, "Error", new ErrorUtils().parseOnFailure(t));
                        }
                    });
                }else{
                    alerts.showDialog(SelectCategories.this, "Error", "Select at least one category.");
                }
            }
        });
    }

    private int getCategoryId(String name) {

        int cat_id;

        switch (name){
            case "music":
                cat_id = 1;
                break;
            case "arts":
                cat_id = 2;
                break;
            case "theatre":
                cat_id = 3;
                break;
            case "festival":
                cat_id = 4;
                break;
            case "party":
                cat_id = 5;
                break;
            case "comedy":
                cat_id = 6;
                break;
            default:
                cat_id = 1;
        }
        return cat_id;
    }
}
