package taafrija.mob.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import taafrija.mob.R;
import taafrija.mob.constants.Constants;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

                startActivity(new Intent(SplashActivity.this, FacebookLogin.class));

                SharedPreferences sp = getSharedPreferences(Constants.PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
                String userID = sp.getString(Constants.FACEBOOK_ID, null);

                if (userID != null) {
                    if (userID.isEmpty()) {
                        // This method will be executed once the timer is over
                        // Start your app main activity
                        Intent intent = new Intent(SplashActivity.this, FacebookLogin.class);
                        startActivity(intent);
                    } else {
                        if(sp.getString(Constants.CATS_SELECTED, null)!=null){
                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                            startActivity(intent);
                        }else {
                            Intent intent = new Intent(SplashActivity.this, SelectCategories.class);
                            startActivity(intent);
                        }
                    }
                } else {
                    Intent intent = new Intent(SplashActivity.this, FacebookLogin.class);
                    startActivity(intent);
                }
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);


    }
}

