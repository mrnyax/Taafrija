package taafrija.mob.constants;

/**
 * Created by mrnyax on 3/25/17.
 */

public class Constants {

    public static final String PREFERENCES_FILE_NAME = "preferenceKey";
    public static final String FACEBOOK_ID = "fb_id";

    public static final String FACEBOOK_USERNAME = "fb_user_name";
    public static final String FACEBOOK_PIC_URL = "fb_profile_pic";

    public static final String CATS_SELECTED = "selected";

}
